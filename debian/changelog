corsix-th (0.68.0-1) unstable; urgency=medium

  * New upstream version 0.68.0
  * Change my email address

 -- Alexandre Detiste <tchet@debian.org>  Tue, 22 Oct 2024 18:23:20 +0200

corsix-th (0.67-1) unstable; urgency=low

  * New upstream version 0.67
  * Update standards version to 4.6.2, no changes needed.
  * Rewrite uscan rule to catch-up with GitHub changes

 -- Alexandre Detiste <alexandre.detiste@gmail.com>  Sun, 13 Aug 2023 03:58:22 +0200

corsix-th (0.66-2) unstable; urgency=medium

  * Fix GitHub uscan integration with lintian-brush

 -- Alexandre Detiste <alexandre.detiste@gmail.com>  Sun, 01 Jan 2023 18:46:14 +0100

corsix-th (0.66-1) unstable; urgency=low

  * New upstream version 0.66
  * build correctly with ffmpeg 5.0.x (Closes: #1013420)
  * works with Lua5.4 lua-lpeg (Closes: #1013161)
  * Refresh default_config.patch
  * define "Rules-Requires-Root: no"
  * depends on fixed lua-lpeg (>= 1.0.2-2)
  * bump Standards-Version, no further change needed
  * refresh lintian overrides for new expected format

 -- Alexandre Detiste <alexandre.detiste@gmail.com>  Tue, 06 Sep 2022 15:08:12 +0200

corsix-th (0.65.1-1) unstable; urgency=medium

  * New upstream version 0.65.1
  * bump lua to 5.4
  * update copyright from LICENSE and git shortlog
  * refresh patches, drop encoding patch applied upstream
  * Use secure URI in Homepage field.

 -- Phil Morrell <debian@emorrp1.name>  Mon, 30 Aug 2021 03:37:55 +0100

corsix-th (0.64-2) unstable; urgency=medium

  * switch to upstream git history
  * use debhelper-compat 13 for automatic nocheck profile
  * allow reprotest to fail (gcc_captures_build_path)
  * fix national-encoding traditional_chinese.lua.
    Thanks to yangfl
  * lintian fixes and version bumps

 -- Phil Morrell <debian@emorrp1.name>  Mon, 07 Dec 2020 22:02:29 +0000

corsix-th (0.64-1) unstable; urgency=medium

  * New upstream version 0.64
  * fix GPG watch file (again)
  * refresh FindSDL2 patch

 -- Phil Morrell <debian@emorrp1.name>  Sun, 09 Aug 2020 04:55:20 +0100

corsix-th (0.63-2) unstable; urgency=medium

  * Team upload
  * Add patch to build successfully against recent SDL2 packaging
    (Closes: #953002)

 -- Simon McVittie <smcv@debian.org>  Sun, 29 Mar 2020 11:58:01 +0100

corsix-th (0.63-1) unstable; urgency=low

  * Team upload.

  [ Alexandre Detiste ]
  * New upstream version 0.63
  * fix GPG part of uscan watch file
  * refresh default config patch
  * upstream now provides a man page, use it

  [ Stephen Kitt ]
  * Use the upstream-installed desktop file, appdata and icon.

 -- Stephen Kitt <skitt@debian.org>  Sun, 17 Nov 2019 21:29:49 +0100

corsix-th (0.62-2) unstable; urgency=medium

  * fix FTBFS since ImageMagick 6.9.10-16 (Closes: 919083)
  * use normal team Maintainer address for DDPO
  * bump Standards-Version, no changes needed
  * bump debhelper-compat to 12

 -- Phil Morrell <debian@emorrp1.name>  Thu, 31 Jan 2019 21:04:00 +0000

corsix-th (0.62-1) unstable; urgency=medium

  [ Phil Morrell ]
  * add gbp.conf
  * add contrib disclaimer
  * lintian testsuite fix, bump metadata
  * allow more variety in signature files
  * fix dh_auto_test nocheck indentation #902392
  * add more verbose flags for S-V 4.2
  * remove obsolete packaging
  * convert manpage to markdown, build with pandoc

  [ Sebastian Ramacher ]
  * New upstream version 0.62
  * debian/: Make cmake install files in correct location.
  * debian/copyright: Update copyright info
  * debian/control:
    - Recommend timidity. (Closes: #904838)
    - Update ffmpeg Build-Depends according to build system.

 -- Phil Morrell <debian@emorrp1.name>  Mon, 13 Aug 2018 14:59:23 +0100

corsix-th (0.61-1) unstable; urgency=medium

  [ Alexandre Detiste ]
  * New upstream version 0.61

  [ Phil Morrell ]
  * add pedantic lintian overrides
  * use https copyright-format url for policy v4.0.0
  * bump Standards-Version, no changes needed
  * add upstream signature to verify releases
  * drop patches released upstream
  * update main copyright authors from LICENSE.txt
  * Change Vcs-* to point to salsa.debian.org

 -- Phil Morrell <debian@emorrp1.name>  Thu, 11 Jan 2018 22:42:02 +0000

corsix-th (0.60-2) unstable; urgency=medium

  * remove obsolete rnc.pp copyright comment.
    Thanks to Alexandre Detiste
  * enable unit tests with newly packaged lua-busted
  * more flexible tag matching in debian/watch
  * bump debhelper compat to 10

 -- Phil Morrell <debian@emorrp1.name>  Tue, 15 Nov 2016 18:08:55 +0000

corsix-th (0.60-1) unstable; urgency=medium

  [ Alexandre Detiste ]
  * New upstream release.

  [ Phil Morrell ]
  * drop all non-debian patches merged upstream
  * backport include_campaigns.patch pushed upstream
  * backport editor_drag_performance.patch for usability
  * drop obsolete lintian-overrides
  * bump Standards-Version
  * add new project website URL
  * add copyright for new campaign by ChrizmanTV
  * update main copyright authors from LICENSE.txt

 -- Phil Morrell <debian@emorrp1.name>  Wed, 22 Jun 2016 23:05:26 +0100

corsix-th (0.50-2) unstable; urgency=medium

  * backport upstream patch to build with ffmpeg 3.0 (Closes: #821415)
  * use v4 watch file to include pre-releases
  * wrap-and-sort -satb

 -- Phil Morrell <debian@emorrp1.name>  Mon, 18 Apr 2016 19:20:40 +0100

corsix-th (0.50-1) unstable; urgency=medium

  * Initial release. (Closes: #610087)

 -- Alexandre Detiste <alexandre.detiste@gmail.com>  Mon, 29 Feb 2016 15:05:38 +0100
